// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App'

import 'mini.css'

Vue.use(VueRouter)

new Vue({
  el: '#app',
  render: h => h(App),
  router: new VueRouter({
    routes: [
      { path: '/fractions', component: () => import('./modules/fractions/Fractions') },
      { path: '/websockets', component: () => import('./modules/websockets/Websockets') }
    ]
  })
})
